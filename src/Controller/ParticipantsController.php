<?php

namespace App\Controller;

use App\Entity\Participants;
use App\Form\ParticipantsFormType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class ParticipantsController extends AbstractController
{
    /**
     * @Route("/participants", name="participants")
     */
    public function index(Request $request): Response{

        $em = $this->getDoctrine()->getRepository(Participants::class);
        $list = $em->findAll();

        $participant = new Participants();
        $form = $this->createForm(ParticipantsFormType::class, $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $participant = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($participant);
             $entityManager->flush();
            $this->addFlash("success",'Membre ajouté ! ');

            return $this->redirectToRoute('participants');
        }



        return $this->render('participants/index.html.twig', [
            'controller_name' => 'ParticipantsController',
            'list'=> $list,
            'form' => $form->createView()
        ]);
    }
}
