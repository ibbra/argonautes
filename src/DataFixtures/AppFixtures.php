<?php

namespace App\DataFixtures;

use App\Entity\Participants;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $noms = ["Eleftheria","Gennadios", "Lysimachos" ];


        for ($i = 0; $i <= 2; $i++) {
            $participant = new Participants();
            $participant->setNom($noms[$i]);

            $manager->persist($participant);

        }


        $manager->flush();

    }
}
